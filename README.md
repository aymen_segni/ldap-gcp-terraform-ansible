# Description

This project spins up a Google Cloud Centos7 instance with Terraform and connects to a preexisting LDAP server using Ansible.
This way you can have a ready to go instance without extra, automatable steps.
To run it on your own machine you need to create a Google Platform account, configure it on your machine, and fill in your custom environmental variables.
Then you can simply run a script and the rest is done for you!